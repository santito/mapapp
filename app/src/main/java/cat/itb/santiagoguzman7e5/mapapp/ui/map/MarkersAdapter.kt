package cat.itb.santiagoguzman7e5.mapapp.ui.map

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cat.itb.santiagoguzman7e5.mapapp.databinding.MarkerItemBinding
import cat.itb.santiagoguzman7e5.mapapp.domain.Marker

class MarkersAdapter(private val list: List<Marker>, private val onClick: (Marker) -> Unit) :
    ListAdapter<Marker, MarkersAdapter.MarkerViewHolder>(MarkerDiffCallback) {
    class MarkerViewHolder(binding: MarkerItemBinding, val onClick: (Marker) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {
        private val markerTextView: TextView = binding.title
        private val markerPicture: ImageView = binding.media
        private var currentMarker: Marker? = null

        init {
            itemView.setOnClickListener {
                currentMarker?.let {
                    onClick(it)
                }
            }
        }

        fun bind(marker: Marker) {
            currentMarker = marker
            markerTextView.text = marker.title
            if (marker.bitmap != null) {
                markerPicture.setImageBitmap(marker.bitmap)
            } else {
                markerPicture.visibility = View.GONE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkerViewHolder {
        val binding = MarkerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MarkerViewHolder(binding, onClick)
    }

    override fun onBindViewHolder(holder: MarkerViewHolder, position: Int) {
        val marker = list[position]
        holder.bind(marker)
    }

    override fun getItemCount(): Int = list.size

    object MarkerDiffCallback : DiffUtil.ItemCallback<Marker>() {
        override fun areItemsTheSame(oldItem: Marker, newItem: Marker): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Marker, newItem: Marker): Boolean {
            return oldItem.id == newItem.id
        }
    }

}