package cat.itb.santiagoguzman7e5.mapapp.domain

import android.graphics.Bitmap
import cat.itb.santiagoguzman7e5.mapapp.utils.BitmapUtils
import com.mapbox.geojson.Point
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PersistedName
import io.realm.kotlin.types.annotations.PrimaryKey
import org.mongodb.kbson.ObjectId

open class Marker(
    @PrimaryKey
    @PersistedName("_id")
    var id: ObjectId,
    var isVisited: Boolean,
    var latitude: Double,
    var longitude: Double,
    @PersistedName("owner_id")
    var ownerId: String,
    var picture: ByteArray?,
    @Index
    var title: String,
) : RealmObject {

    val point: Point
        get() = Point.fromLngLat(longitude, latitude)

    val bitmap: Bitmap?
        get() = picture?.let { BitmapUtils.convertByteArrayToBitmap(it) }

    constructor() : this(
        id = ObjectId(),
        isVisited = false,
        latitude = 0.0,
        longitude = 0.0,
        ownerId = "",
        picture = null,
        title = ""
    )

    override fun toString(): String {
        return title
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is Marker) return false
        if (this.id != other.id) return false
        if (this.isVisited != other.isVisited) return false
        if (this.title != other.title) return false
        if (this.ownerId != other.ownerId) return false
        if (this.point != other.point) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 42 * result + isVisited.hashCode()
        result = 42 * result + title.hashCode()
        result = 42 * result + ownerId.hashCode()
        result = 42 * result + point.hashCode()
        return result
    }
}
