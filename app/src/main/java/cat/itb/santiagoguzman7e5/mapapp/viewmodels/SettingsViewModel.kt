package cat.itb.santiagoguzman7e5.mapapp.viewmodels

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.savedstate.SavedStateRegistryOwner
import cat.itb.santiagoguzman7e5.mapapp.data.SubscriptionType
import cat.itb.santiagoguzman7e5.mapapp.data.SyncRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

sealed class SettingsEvent {
    object LogOut : SettingsEvent()
    class Info(val message: String) : SettingsEvent()
    class Error(val message: String, val throwable: Throwable) : SettingsEvent()
}

sealed class SubscriptionTypeEvent {
    class Info(val message: String) : SubscriptionTypeEvent()
    object PermissionsEvent : SubscriptionTypeEvent()
    class Error(val message: String, val throwable: Throwable) : SubscriptionTypeEvent()
}

class SettingsViewModel(
    private val repository: SyncRepository,
) : ViewModel() {

    private val _offlineMode: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val offlineMode: StateFlow<Boolean>
        get() = _offlineMode

    private val _settingsEvent: MutableSharedFlow<SettingsEvent> = MutableSharedFlow()
    val settingsEvent: Flow<SettingsEvent>
        get() = _settingsEvent

    fun goOffline() {
        _offlineMode.value = true
        repository.pauseSync()
    }

    fun goOnline() {
        _offlineMode.value = false
        repository.resumeSync()
    }

    fun logOut() {
        viewModelScope.launch {
            _settingsEvent.emit(SettingsEvent.LogOut)
        }
    }

    fun error(errorEvent: SettingsEvent.Error) {
        viewModelScope.launch {
            _settingsEvent.emit(errorEvent)
        }
    }

    private val _subscriptionType: MutableStateFlow<SubscriptionType> =
        MutableStateFlow(repository.getActiveSubscriptionType())
    val subscriptionType: StateFlow<SubscriptionType>
        get() = _subscriptionType

    private val _subscriptionTypeEvent: MutableSharedFlow<SubscriptionTypeEvent> =
        MutableSharedFlow()
    val subscriptionTypeEvent: Flow<SubscriptionTypeEvent>
        get() = _subscriptionTypeEvent

    fun updateSubscription(subscriptionType: SubscriptionType) {
        CoroutineScope(Dispatchers.IO).launch {
            runCatching {
                repository.updateSubscriptions(subscriptionType)
                _subscriptionType.value = subscriptionType
            }.onSuccess {
                withContext(Dispatchers.Main) {
                    _subscriptionTypeEvent.emit(SubscriptionTypeEvent.Info("Successfully switched to '${subscriptionType.name}'"))
                }
            }.onFailure {
                withContext(Dispatchers.Main) {
                    _subscriptionTypeEvent.emit(SubscriptionTypeEvent.Error("There was an error while switching to '${subscriptionType.name}'",
                        it))
                }
            }
        }
    }

    fun showOfflineMessage() {
        viewModelScope.launch {
            _subscriptionTypeEvent.emit(SubscriptionTypeEvent.PermissionsEvent)
        }
    }

    @Suppress("UNCHECKED_CAST")
    companion object {
        fun factory(
            repository: SyncRepository,
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null,
        ): AbstractSavedStateViewModelFactory {
            return object : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
                override fun <T : ViewModel> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle,
                ): T {
                    return SettingsViewModel(repository) as T
                }
            }
        }
    }
}