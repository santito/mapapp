package cat.itb.santiagoguzman7e5.mapapp.ui.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import cat.itb.santiagoguzman7e5.mapapp.R
import cat.itb.santiagoguzman7e5.mapapp.databinding.MarkerBottomSheetBinding
import cat.itb.santiagoguzman7e5.mapapp.domain.Marker
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.MarkerViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MarkerBottomSheet(private val marker: Marker) : BottomSheetDialogFragment() {

    private var _binding: MarkerBottomSheetBinding? = null
    private val binding get() = _binding!!

    private lateinit var markerViewModel: MarkerViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = MarkerBottomSheetBinding.inflate(inflater, container, false)
        markerViewModel =
            activity?.let { activity -> ViewModelProvider(activity)[MarkerViewModel::class.java] }!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMarkerScaffold()
    }

    private fun setupMarkerScaffold() {
        val image = binding.media
        val title = binding.title
        val owner = binding.ownerChip
        val checkbox = binding.visited
        val button = binding.deleteButton

        with(image) {
            if (marker.bitmap != null) {
                setImageBitmap(marker.bitmap)
            } else {
                visibility = View.GONE
            }
        }
        with(title) {
            text = marker.title
        }
        with(owner) {
            if (markerViewModel.isMarkerMine(marker)) {
                text = getString(R.string.mine)
            } else {
                visibility = View.GONE
            }
        }

        with(checkbox) {
            isChecked = marker.isVisited
            setOnCheckedChangeListener { _, _ ->
                if (markerViewModel.isMarkerMine(marker)) {
                    markerViewModel.toggleIsVisited(marker)
                } else {
                    error = getString(R.string.permissions_error)
                    markerViewModel.showPermissionsMessage()
                    this@MarkerBottomSheet.dismiss()
                }
            }
        }

        with(button) {
            setOnClickListener {
                markerViewModel.deleteMarker(marker)
                this@MarkerBottomSheet.dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
