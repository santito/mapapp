package cat.itb.santiagoguzman7e5.mapapp.ui.map

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts.PickVisualMedia
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import cat.itb.santiagoguzman7e5.mapapp.databinding.AddMarkerDialogBinding
import cat.itb.santiagoguzman7e5.mapapp.utils.BitmapUtils
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.AddMarkerViewModel
import com.mapbox.geojson.Point
import kotlinx.coroutines.launch

class AddMarkerDialog(private val point: Point) : DialogFragment() {

    private var _binding: AddMarkerDialogBinding? = null
    private val binding get() = _binding!!

    private lateinit var addMarkerViewModel: AddMarkerViewModel

    private val pickVisualMedia
        get() = (activity as MapActivity).pickMedia

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = AddMarkerDialogBinding.inflate(inflater, container, false)
        addMarkerViewModel =
            activity?.let { activity -> ViewModelProvider(activity)[AddMarkerViewModel::class.java] }!!
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAddMarkerScaffold()
    }

    private fun setupAddMarkerScaffold() = lifecycleScope.launch {
        addMarkerViewModel.updateMarkerPoint(point)
        setupTitleCollector()
        setupImageCollector()
        val confirmButton = binding.confirmButton
        val dismissButton = binding.dismissButton

        with(confirmButton) {
            setOnClickListener {
                addMarkerViewModel.addMarker()
                dialog?.dismiss()
            }
        }
        with(dismissButton) {
            setOnClickListener {
                dialog?.dismiss()
            }
        }
    }

    private fun setupImageCollector() = lifecycleScope.launch {
        val image = binding.addImage.apply {
            setOnClickListener {
                pickVisualMedia
                    .launch(PickVisualMediaRequest(PickVisualMedia.ImageOnly))
            }
        }
        addMarkerViewModel.markerImage.collect {
            with(image) {
                if (it.isNotEmpty()) {
                    setImageBitmap(BitmapUtils.convertByteArrayToBitmap(it))
                }
            }
        }
    }

    private fun setupTitleCollector() = lifecycleScope.launch {
        val textField = binding.textField.apply {
            setText(addMarkerViewModel.markerTitle.value)
        }
        addMarkerViewModel.markerTitle.collect {
            with(textField) {
                addTextChangedListener {
                    addMarkerViewModel.updateMarkerTitle(it.toString())
                }
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        addMarkerViewModel.cleanUpAndClose()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}