package cat.itb.santiagoguzman7e5.mapapp.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import cat.itb.santiagoguzman7e5.mapapp.ui.map.MapActivity
import cat.itb.santiagoguzman7e5.mapapp.R
import cat.itb.santiagoguzman7e5.mapapp.TAG
import cat.itb.santiagoguzman7e5.mapapp.app
import cat.itb.santiagoguzman7e5.mapapp.databinding.ActivityLoginBinding
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.EventSeverity
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.LoginAction
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.LoginEvent
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.LoginViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Fast-track map screen if we are logged in
        if (app.currentUser != null) {
            startActivity(Intent(this, MapActivity::class.java))
            finish()
            return
        }

        setupNavigationLoggingEvents()
        setupLoginScaffold()
    }

    private fun setupLoginScaffold() = lifecycleScope.launch {
        val email = binding.emailField.apply {
            setText(loginViewModel.state.value.email)
        }
        val password = binding.passwordField.apply {
            setText(loginViewModel.state.value.password)
        }
        val accountButton = binding.button
        val switchText = binding.text

        loginViewModel.state
            .collect { state ->
                // Email field
                with(email) {
                    isEnabled = state.enabled
                    addTextChangedListener {
                        loginViewModel.setEmail(it.toString())
                    }
                }
                // Password field
                with(password) {
                    isEnabled = state.enabled
                    addTextChangedListener {
                        loginViewModel.setPassword(it.toString())
                    }
                }
                // Login/create account button
                with(accountButton) {
                    isEnabled = state.enabled
                    setOnClickListener {
                        when (state.action) {
                            LoginAction.LOGIN -> loginViewModel.login(
                                state.email,
                                state.password
                            )
                            LoginAction.CREATE_ACCOUNT -> loginViewModel.createAccount(
                                state.email,
                                state.password
                            )
                        }
                    }
                    text = when (state.action) {
                        LoginAction.CREATE_ACCOUNT -> getString(R.string.create_account)
                        LoginAction.LOGIN -> getString(R.string.log_in)
                    }
                }
                // Switch between login and create user
                with(switchText) {
                    setOnClickListener {
                        when (state.action) {
                            LoginAction.LOGIN -> loginViewModel.switchToAction(LoginAction.CREATE_ACCOUNT)
                            LoginAction.CREATE_ACCOUNT -> loginViewModel.switchToAction(LoginAction.LOGIN)
                        }
                    }
                    text = when (state.action) {
                        LoginAction.CREATE_ACCOUNT -> getString(R.string.already_have_account)
                        LoginAction.LOGIN -> getString(R.string.does_not_have_account)
                    }
                }
            }
    }

    private fun setupNavigationLoggingEvents() = lifecycleScope
        .launch {
            // Subscribe to navigation and message-logging events
            loginViewModel.event
                .collect { event ->
                    when (event) {
                        is LoginEvent.GoToTasks -> {
                            event.process()

                            val intent = Intent(this@LoginActivity, MapActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                        is LoginEvent.ShowMessage -> event.process()
                    }
                }
        }

    private fun LoginEvent.process() {
        when (severity) {
            EventSeverity.INFO -> Log.i(TAG(), message)
            EventSeverity.ERROR -> {
                Log.e(TAG(), message)
                Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
            }
        }
    }
}