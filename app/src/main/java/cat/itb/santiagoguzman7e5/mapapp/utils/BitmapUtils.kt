package cat.itb.santiagoguzman7e5.mapapp.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources


/**
 * Utility class to work with bitmaps and drawables.
 */
object BitmapUtils {

    /**
     * Convert given drawable id to bitmap.
     */
    fun bitmapFromDrawableRes(context: Context?, @DrawableRes resourceId: Int) =
        convertDrawableToBitmap(context?.let {
            AppCompatResources.getDrawable(it, resourceId)
        })

    private fun convertDrawableToBitmap(sourceDrawable: Drawable?): Bitmap? {
        return sourceDrawable?.let {
            if (sourceDrawable is BitmapDrawable) {
                sourceDrawable.bitmap
            } else {
                // copying drawable object to not manipulate on the same reference
                val constantState = sourceDrawable.constantState ?: return null
                val drawable = constantState.newDrawable().mutate()
                val bitmap: Bitmap = Bitmap.createBitmap(
                    drawable.intrinsicWidth, drawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(bitmap)
                drawable.setBounds(0, 0, canvas.width, canvas.height)
                drawable.draw(canvas)
                bitmap
            }
        }
    }

    fun convertByteArrayToBitmap(src: ByteArray): Bitmap? {
        return BitmapFactory.decodeByteArray(src, 0, src.size)
    }
}