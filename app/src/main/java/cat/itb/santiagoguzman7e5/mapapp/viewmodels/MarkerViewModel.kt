package cat.itb.santiagoguzman7e5.mapapp.viewmodels

import android.os.Bundle
import androidx.lifecycle.*
import androidx.savedstate.SavedStateRegistryOwner
import cat.itb.santiagoguzman7e5.mapapp.data.SyncRepository
import cat.itb.santiagoguzman7e5.mapapp.domain.Marker
import io.realm.kotlin.notifications.InitialResults
import io.realm.kotlin.notifications.ResultsChange
import io.realm.kotlin.notifications.UpdatedResults
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

object MarkerViewEvent

class MarkerViewModel constructor(
    private val repository: SyncRepository,
) : ViewModel() {

    private val _markerListState: MutableLiveData<MutableList<Marker>> =
        MutableLiveData(mutableListOf())
    val markerListState: Flow<List<Marker>>
        get() = _markerListState.asFlow()

    private val _event: MutableSharedFlow<MarkerViewEvent> = MutableSharedFlow()
    val event: Flow<MarkerViewEvent>
        get() = _event

    init {
        viewModelScope.launch {
            repository.getMarkerList()
                .collect { event: ResultsChange<Marker> ->
                    when (event) {
                        is InitialResults -> {
                            _markerListState.value = _markerListState.value?.apply {
                                clear()
                                addAll(event.list)
                            }
                        }
                        is UpdatedResults -> {
                            if (event.deletions.isNotEmpty() && _markerListState.value?.isNotEmpty() == true) {
                                event.deletions.reversed().forEach {
                                    _markerListState.value =
                                        _markerListState.value?.apply { removeAt(it) }
                                }
                            }
                            if (event.insertions.isNotEmpty()) {
                                event.insertions.forEach {
                                    _markerListState.value = _markerListState.value?.apply {
                                        add(it, event.list[it])
                                    }
                                }
                            }
                            if (event.changes.isNotEmpty()) {
                                event.changes.forEach {
                                    _markerListState.value = _markerListState.value?.apply {
                                        removeAt(it)
                                        add(it, event.list[it])
                                    }
                                }
                            }
                        }
                        else -> Unit // No-op
                    }
                }
        }
    }

    fun toggleIsVisited(marker: Marker) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.toggleIsVisited(marker)
        }
    }

    fun showPermissionsMessage() {
        viewModelScope.launch {
            _event.emit(MarkerViewEvent)
        }
    }

    fun deleteMarker(marker: Marker) {
        CoroutineScope(Dispatchers.IO).launch {
            if (repository.isMarkerMine(marker)) {
                runCatching {
                    repository.deleteMarker(marker)
                }
            } else {
                showPermissionsMessage()
            }
        }
    }

    fun isMarkerMine(marker: Marker): Boolean = repository.isMarkerMine(marker)

    @Suppress("UNCHECKED_CAST")
    companion object {
        fun factory(
            repository: SyncRepository,
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null,
        ): AbstractSavedStateViewModelFactory {
            return object : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
                override fun <T : ViewModel> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle,
                ): T {
                    return MarkerViewModel(repository) as T
                }
            }
        }
    }
}