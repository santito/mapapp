package cat.itb.santiagoguzman7e5.mapapp.ui.map

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import cat.itb.santiagoguzman7e5.mapapp.R
import cat.itb.santiagoguzman7e5.mapapp.TAG
import cat.itb.santiagoguzman7e5.mapapp.app
import cat.itb.santiagoguzman7e5.mapapp.data.RealmSyncRepository
import cat.itb.santiagoguzman7e5.mapapp.databinding.ActivityMapBinding
import cat.itb.santiagoguzman7e5.mapapp.ui.login.LoginActivity
import cat.itb.santiagoguzman7e5.mapapp.utils.LocationPermissionHelper
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class MapActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMapBinding

    private val repository = RealmSyncRepository { _, error ->
        // Sync errors come from a background thread so route the Toast through the UI thread
        lifecycleScope.launch {
            // Catch write permission errors and notify user. This is just a 2nd line of defense
            // since we prevent users from modifying someone else's tasks
            if (error.message?.contains("CompensatingWrite") == true) {
                Snackbar.make(binding.root,
                    getString(R.string.permissions_error),
                    Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private val settingsViewModel: SettingsViewModel by viewModels {
        SettingsViewModel.factory(repository, this)
    }

    private val markerViewModel: MarkerViewModel by viewModels {
        MarkerViewModel.factory(repository, this)
    }

    private val addMarkerViewModel: AddMarkerViewModel by viewModels {
        AddMarkerViewModel.factory(repository, this)
    }

    val pickMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        // Callback is invoked after the user selects a media item or closes the
        // photo picker.
        val inputData = uri?.let { contentResolver.openInputStream(it)?.readBytes() }
        if (inputData != null) {
            addMarkerViewModel.updateMarkerImage(inputData)
        }
    }

    private val locationPermissionHelper: LocationPermissionHelper =
        LocationPermissionHelper(this@MapActivity) {
            initMapComponent()
            initSettingsComponent()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (app.currentUser == null) {
            startActivity(Intent(this@MapActivity, LoginActivity::class.java))
            finish()
            return
        }
        binding = ActivityMapBinding.inflate(layoutInflater)
        setContentView(binding.root)
        locationPermissionHelper.checkPermissions()
    }

    private fun initMapComponent() {
        setupMapEventCollector()
        setupAddMarkerEventCollector()
    }

    private fun setupMapEventCollector() = lifecycleScope.launch {
        markerViewModel.event
            .collect {
                Log.i(TAG(),
                    "Tried to modify or remove a task that doesn't belong to the current user.")
                Snackbar.make(
                    binding.root,
                    getString(R.string.permissions_warning),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
    }

    private fun setupAddMarkerEventCollector() = lifecycleScope.launch {
        addMarkerViewModel.addMarkerEvent
            .collect { fabEvent ->
                when (fabEvent) {
                    is AddMarkerEvent.Error ->
                        Log.e(TAG(), "${fabEvent.message}: ${fabEvent.throwable.message}")
                    is AddMarkerEvent.Info ->
                        Log.e(TAG(), fabEvent.message)
                }
            }
    }

    private fun initSettingsComponent() {
        setupSettingsEventCollector()
        setupSubscriptionEventCollector()
    }

    private fun setupSettingsEventCollector() = lifecycleScope.launch {
        settingsViewModel.settingsEvent
            .collect { settingsEvent ->
                when (settingsEvent) {
                    SettingsEvent.LogOut -> {
                        startActivity(Intent(this@MapActivity, LoginActivity::class.java))
                        finish()
                    }
                    is SettingsEvent.Info ->
                        Log.e(TAG(), settingsEvent.message)
                    is SettingsEvent.Error ->
                        Log.e(TAG(), "${settingsEvent.message}: ${settingsEvent.throwable.message}")
                }
            }
    }

    private fun setupSubscriptionEventCollector() = lifecycleScope.launch {
        settingsViewModel.subscriptionTypeEvent
            .collect { subscriptionTypeEvent ->
                when (subscriptionTypeEvent) {
                    is SubscriptionTypeEvent.Error ->
                        Log.e(TAG(),
                            "${subscriptionTypeEvent.message}: ${subscriptionTypeEvent.throwable.message}")
                    is SubscriptionTypeEvent.Info ->
                        Log.i(TAG(), subscriptionTypeEvent.message)
                    SubscriptionTypeEvent.PermissionsEvent -> {
                        Log.i(TAG(), "Tried to switch subscription while offline.")
                        Snackbar.make(
                            binding.root,
                            getString(R.string.unable_to_switch),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun onDestroy() {
        super.onDestroy()
        repository.close()
    }
}