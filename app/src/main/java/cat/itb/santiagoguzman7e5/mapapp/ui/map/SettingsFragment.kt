package cat.itb.santiagoguzman7e5.mapapp.ui.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import cat.itb.santiagoguzman7e5.mapapp.R
import cat.itb.santiagoguzman7e5.mapapp.app
import cat.itb.santiagoguzman7e5.mapapp.data.SubscriptionType
import cat.itb.santiagoguzman7e5.mapapp.databinding.FragmentSettingsBinding
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.SettingsEvent
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.SettingsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SettingsFragment : Fragment() {

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    private lateinit var settingsViewModel: SettingsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        settingsViewModel =
            activity?.let { activity -> ViewModelProvider(activity)[SettingsViewModel::class.java] }!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.backButton.setOnClickListener {
            findNavController().navigate(R.id.action_settings_to_map)
        }
        setupOwnerListener()
        setupOfflineListener()
        setupLogoutListener()
    }

    private fun setupLogoutListener() = lifecycleScope.launch {
        val logoutButton = binding.logoutButton
        logoutButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                runCatching {
                    app.currentUser?.logOut()
                }.onSuccess {
                    settingsViewModel.logOut()
                }.onFailure {
                    settingsViewModel.error(SettingsEvent.Error("Log out failed", it))
                }
            }
        }
    }

    private fun setupOfflineListener() = lifecycleScope.launch {
        val offlineSwitch = binding.offlineSwitch
        settingsViewModel.offlineMode.collect {
            offlineSwitch.isChecked = it
            offlineSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
                when (isChecked) {
                    false -> settingsViewModel.goOnline()
                    true -> settingsViewModel.goOffline()
                }
            }
        }
    }

    private fun setupOwnerListener() = lifecycleScope.launch {
        val ownerSwitch = binding.ownerSwitch
        settingsViewModel.subscriptionType.collect {
            ownerSwitch.isChecked = when (it) {
                SubscriptionType.MINE -> false
                SubscriptionType.ALL -> true
            }
            ownerSwitch.setOnCheckedChangeListener { _, _ ->
                if (settingsViewModel.offlineMode.value) {
                    settingsViewModel.showOfflineMessage()
                } else {
                    val updatedSubscriptionType = when (it) {
                        SubscriptionType.MINE -> SubscriptionType.ALL
                        SubscriptionType.ALL -> SubscriptionType.MINE
                    }
                    settingsViewModel.updateSubscription(updatedSubscriptionType)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}