package cat.itb.santiagoguzman7e5.mapapp.viewmodels

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import cat.itb.santiagoguzman7e5.mapapp.data.SyncRepository
import com.mapbox.geojson.Point
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

sealed class AddMarkerEvent {
    class Info(val message: String) : AddMarkerEvent()
    class Error(val message: String, val throwable: Throwable) : AddMarkerEvent()
}

class AddMarkerViewModel(
    private val repository: SyncRepository,
) : ViewModel() {


    private val _markerTitle: MutableStateFlow<String> = MutableStateFlow("")
    val markerTitle: StateFlow<String>
        get() = _markerTitle

    private val _markerPoint: MutableStateFlow<Point> = MutableStateFlow(Point.fromLngLat(0.0, 0.0))
    private val markerPoint: StateFlow<Point>
        get() = _markerPoint

    private val _markerImage: MutableStateFlow<ByteArray> = MutableStateFlow(byteArrayOf())
    val markerImage: StateFlow<ByteArray>
        get() = _markerImage

    private val _addMarkerEvent: MutableSharedFlow<AddMarkerEvent> = MutableSharedFlow()
    val addMarkerEvent: Flow<AddMarkerEvent>
        get() = _addMarkerEvent


    fun updateMarkerTitle(markerTitle: String) {
        _markerTitle.value = markerTitle
    }

    fun updateMarkerPoint(markerPoint: Point) {
        _markerPoint.value = markerPoint
    }

    fun updateMarkerImage(byteArray: ByteArray) {
        _markerImage.value = byteArray
    }


    fun addMarker() {
        CoroutineScope(Dispatchers.IO).launch {
            runCatching {
                repository.addMarker(markerTitle.value,
                    markerPoint.value.longitude(),
                    markerPoint.value.latitude(),
                    markerImage.value)
            }.onSuccess {
                withContext(Dispatchers.Main) {
                    _addMarkerEvent.emit(AddMarkerEvent.Info("Task '$markerPoint' added successfully."))
                }
            }.onFailure {
                withContext(Dispatchers.Main) {
                    _addMarkerEvent.emit(AddMarkerEvent.Error("There was an error while adding the task '$markerPoint'",
                        it))
                }
            }
            cleanUpAndClose()
        }
    }

    fun cleanUpAndClose() {
        _markerTitle.value = ""
        _markerPoint.value = Point.fromLngLat(0.0, 0.0)
        _markerImage.value = byteArrayOf()
    }

    @Suppress("UNCHECKED_CAST")
    companion object {
        fun factory(
            repository: SyncRepository,
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null,
        ): AbstractSavedStateViewModelFactory {
            return object : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
                override fun <T : ViewModel> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle,
                ): T {
                    return AddMarkerViewModel(repository) as T
                }
            }
        }
    }
}