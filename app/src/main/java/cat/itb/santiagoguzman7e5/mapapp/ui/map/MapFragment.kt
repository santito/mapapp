package cat.itb.santiagoguzman7e5.mapapp.ui.map

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cat.itb.santiagoguzman7e5.mapapp.R
import cat.itb.santiagoguzman7e5.mapapp.TAG
import cat.itb.santiagoguzman7e5.mapapp.databinding.FragmentMapBinding
import cat.itb.santiagoguzman7e5.mapapp.domain.Marker
import cat.itb.santiagoguzman7e5.mapapp.utils.BitmapUtils
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.AddMarkerViewModel
import cat.itb.santiagoguzman7e5.mapapp.viewmodels.MarkerViewModel
import com.mapbox.geojson.Point
import com.mapbox.maps.CameraOptions
import com.mapbox.maps.MapView
import com.mapbox.maps.dsl.cameraOptions
import com.mapbox.maps.plugin.animation.MapAnimationOptions.Companion.mapAnimationOptions
import com.mapbox.maps.plugin.animation.flyTo
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManager
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.compass.compass
import com.mapbox.maps.plugin.gestures.OnMapLongClickListener
import com.mapbox.maps.plugin.gestures.addOnMapLongClickListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.maps.plugin.scalebar.scalebar
import kotlinx.coroutines.launch
import java.util.*

class MapFragment : Fragment(), OnMapLongClickListener {

    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!!

    private lateinit var markerViewModel: MarkerViewModel
    private lateinit var addMarkerViewModel: AddMarkerViewModel
    private lateinit var mapView: MapView
    private lateinit var pointAnnotationManager: PointAnnotationManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentMapBinding.inflate(inflater, container, false)
        markerViewModel =
            activity?.let { activity -> ViewModelProvider(activity)[MarkerViewModel::class.java] }!!
        addMarkerViewModel =
            activity?.let { activity -> ViewModelProvider(activity)[AddMarkerViewModel::class.java] }!!
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = binding.mapView
        pointAnnotationManager = mapView.annotations.createPointAnnotationManager()
        setupSearchBar()
        onMapReady()
    }

    private fun onMapReady() {
        mapView
            .apply {
                location.updateSettings {
                    enabled = true
                    pulsingEnabled = true
                }
                scalebar.enabled = false
                compass.enabled = false
                getMapboxMap().apply {
                    setCamera(
                        CameraOptions.Builder()
                            .center(
                                Point.fromLngLat(
                                    41.45341443806144, 2.1862882712409957
                                )
                            )
                            .zoom(0.42)
                            .build()
                    )
                    loadStyleUri(
                        getString(R.string.mapbox_style_URL)
                    )
                    // After the style is loaded, initialize the other features.
                    {
                        addOnMapLongClickListener(this@MapFragment)
                        setupMarkersCollector()
                    }
                }
            }

    }

    private fun setupMarkersCollector() = lifecycleScope.launch {
        val markerList = markerViewModel.markerListState
        val recycler: RecyclerView = binding.searchRecycler
        val search: EditText = binding.searchView.editText

        markerList.collect { items ->
            mapView.annotations.removeAnnotationManager(pointAnnotationManager)
            items.forEach {
                search.addTextChangedListener { text ->
                    items.filter { item ->
                        item.title.lowercase(Locale.ROOT)
                            .contains(text.toString().lowercase(Locale.ROOT))
                    }.apply {
                        recycler.adapter = MarkersAdapter(this) { marker ->
                            showMarkerSheet(marker)
                        }
                    }
                }
                addAnnotationToMap(it)
            }
        }
    }


    private fun setupSearchBar() {
        binding.searchBar.apply {
            setOnMenuItemClickListener { menuItem ->
                findNavController().navigate(R.id.action_map_to_settings)
                menuItem.isEnabled
            }
            binding.searchView.setupWithSearchBar(this)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapLongClick(point: Point): Boolean {
        val dialog = AddMarkerDialog(point)
        activity?.let { activity -> dialog.show(activity.supportFragmentManager, TAG()) }
        return true
    }

    private fun addAnnotationToMap(marker: Marker) {
        BitmapUtils.bitmapFromDrawableRes(
            context,
            R.drawable.ic_marker
        )?.let { bitmap ->
            val pointAnnotationOptions: PointAnnotationOptions = PointAnnotationOptions()
                .withPoint(marker.point)
                .withIconImage(bitmap)

            val annotationApi = mapView.annotations
            pointAnnotationManager = annotationApi.createPointAnnotationManager()
            pointAnnotationManager.apply {
                addClickListener {
                    showMarkerSheet(marker)
                }
                create(pointAnnotationOptions)
            }
        }
    }

    private fun showMarkerSheet(marker: Marker): Boolean {
        val searchView = binding.searchView
        searchView.hide()
        setIndicatorPositionChanged(marker.point) {
            val markerBottomSheet = MarkerBottomSheet(marker)
            activity?.let { activity ->
                markerBottomSheet
                    .showNow(activity.supportFragmentManager, TAG())
            }
        }
        return true
    }

    private fun setIndicatorPositionChanged(point: Point, onEnd: () -> Unit?) {
        mapView.getMapboxMap().flyTo(
            cameraOptions {
                center(point)
                zoom(12.5)
            },
            mapAnimationOptions {
                duration(2_000)
                animatorListener(object : Animator.AnimatorListener {
                    override fun onAnimationStart(animation: Animator) {
                        return
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        onEnd()
                    }

                    override fun onAnimationCancel(animation: Animator) {
                        return
                    }

                    override fun onAnimationRepeat(animation: Animator) {
                        return
                    }
                })
            }
        )
        mapView.gestures.focalPoint = mapView.getMapboxMap().pixelForCoordinate(point)
    }

}