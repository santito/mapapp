package cat.itb.santiagoguzman7e5.mapapp.utils

import android.app.Activity
import android.view.View
import cat.itb.santiagoguzman7e5.mapapp.R
import com.google.android.material.snackbar.Snackbar
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager

class LocationPermissionHelper(val activity: Activity, val onPermissionGranted: () -> Unit) {

    private val permissionsManager: PermissionsManager =
        PermissionsManager(object : PermissionsListener {
            override fun onExplanationNeeded(permissionsToExplain: List<String>) {
                val contextView = activity.findViewById<View>(R.id.container)

                Snackbar.make(
                    contextView, "You need to accept location permissions.",
                    Snackbar.LENGTH_SHORT
                ).show()
            }

            override fun onPermissionResult(granted: Boolean) {
                if (granted) {
                    onPermissionGranted()
                } else {
                    activity.finish()
                }
            }
        })


    fun checkPermissions() {
        if (PermissionsManager.areLocationPermissionsGranted(activity)) {
            onPermissionGranted()
        } else {
            permissionsManager.requestLocationPermissions(activity)
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}