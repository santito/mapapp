# MapApp

Maps demo app for Android

## Configuration

Ensure `app/src/main/res/values/atlasConfig.xml` exists and contains the following properties:

- **realm_app_id:** Atlas App Services App ID.
- **realm_base_url** the App Services backend URL. This should be https://realm.mongodb.com in most cases.

And also ensure that `app/src/main/res/values/mapboxConfig.xml` exists and contains the following properties:
- **mapbox_access_token:** Mapbox Services Access Token.

## Authors and acknowledgment
@santito

## License
For open source projects, say how it is licensed.

